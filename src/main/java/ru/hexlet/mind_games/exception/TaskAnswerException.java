package ru.hexlet.mind_games.exception;

public class TaskAnswerException extends RuntimeException{

    public TaskAnswerException(String message) {
        super(message);
    }

    public TaskAnswerException() {
        super("The task answer is bad!");
    }

}
