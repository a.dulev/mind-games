package ru.hexlet.mind_games.model.game;

import ru.hexlet.mind_games.exception.TaskAnswerException;
import ru.hexlet.mind_games.exception.TaskQuestionException;
import ru.hexlet.mind_games.model.task.EvenTask;
import ru.hexlet.mind_games.sevice.AnswerValidator;

public class EvenGame implements Game<EvenTask>{

    @Override
    public EvenTask instanceTask() {
        return new EvenTask();
    }

    @Override
    public boolean checkResult(EvenTask task) {
        validateQuestion(task);
        validateAnswer(task);
        String rightAnswer = (task.getCheckValue()%2==0) ? "yes" : "no";
        return task.getAnswer().equalsIgnoreCase(rightAnswer);
    }

    @Override
    public String getName() {
        return "Even number";
    }

    @Override
    public int getId() {
        return 2;
    }

    @Override
    public void validateQuestion(EvenTask task) {
        if (task==null || task.getQuestion()==null || task.getQuestion().isBlank()
                || task.getCheckValue()==null || task.getCheckValue()<=0)
            throw new TaskQuestionException();
    }

    @Override
    public void validateAnswer(EvenTask task) throws TaskAnswerException {
        AnswerValidator.yesOrNo(task);
    }
}
