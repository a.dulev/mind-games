package ru.hexlet.mind_games.model.game;

import ru.hexlet.mind_games.model.task.Task;
import ru.hexlet.mind_games.sevice.TaskValidator;

/**
 * Описание игры
 */
public interface Game<T extends Task> extends TaskValidator<T> {

    /**
     * Метод создает новое задание
     * @return - новое задание
     */
    T instanceTask();

    /**
     * Метод проверяет результат выполнения задания
     * @param task - задание для проверки
     * @return - результат проверки
     */
    boolean checkResult(T task);

    /**
     * Метод возвращает имя игры
     * @return - имя игры
     */
    String getName();

    /**
     * Метод возвращает идентификатор игры
     * @return - идентификатор игры
     */
    int getId();
}
