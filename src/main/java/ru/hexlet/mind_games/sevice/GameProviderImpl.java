package ru.hexlet.mind_games.sevice;

import ru.hexlet.mind_games.model.game.EvenGame;
import ru.hexlet.mind_games.model.game.Game;
import ru.hexlet.mind_games.model.game.MaxSameDivGame;
import ru.hexlet.mind_games.model.game.PrimeGame;
import ru.hexlet.mind_games.model.game.ProgressionGame;
import ru.hexlet.mind_games.model.game.SumGame;

import java.util.ArrayList;
import java.util.List;

public class GameProviderImpl implements GameProvider {

    private List<Game> registryList = new ArrayList<>();

    public GameProviderImpl() {
        registryList.add(new SumGame());
        registryList.add(new EvenGame());
        registryList.add(new ProgressionGame());
        registryList.add(new PrimeGame());
        registryList.add(new MaxSameDivGame());
    }

    @Override
    public Game getById(int id) {
         return registryList.stream()
                 .filter(game -> game.getId() == id)
                 .findFirst().orElse(null);
    }

    @Override
    public List<Game> getAll() {
        return registryList.stream().toList();
    }
}
