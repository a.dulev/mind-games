package ru.hexlet.mind_games.sevice;

import ru.hexlet.mind_games.exception.TaskAnswerException;
import ru.hexlet.mind_games.exception.TaskQuestionException;
import ru.hexlet.mind_games.model.task.Task;

/**
 * Клаас описывает валидацию задания
 * @param <T>
 */
public interface TaskValidator<T extends Task> {
    /**
     * Метод проверяет корректность поставленного задания
     * @param task - задача
     * @throws TaskQuestionException
     */
    void validateQuestion(T task) throws TaskQuestionException;

    /**
     * Метод проверяет корректность ответа
     * @param task - задача
     * @throws TaskAnswerException
     */
    void validateAnswer(T task) throws TaskAnswerException;
}
